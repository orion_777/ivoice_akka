package com.lightbend.akka.sample;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.*;

public class GreeterBot extends AbstractBehavior<Greeter.Greeted> {
	
	private final String question;
	private final String[] commandClose;

    public static Behavior<Greeter.Greeted> create(String question, String[] commandClose) {    	
        return Behaviors.setup(context -> new GreeterBot(context, question, commandClose));
    }

    private GreeterBot(ActorContext<Greeter.Greeted> context, String question, String[] commandClose) {
        super(context);
        this.question = question;
        this.commandClose = commandClose;
    }

    @Override
    public Receive<Greeter.Greeted> createReceive() {
        return newReceiveBuilder().onMessage(Greeter.Greeted.class, this::onGreeted).build();
    }

    private Behavior<Greeter.Greeted> onGreeted(Greeter.Greeted message) {
    	
    	try{
    		
    		//send request
    		String urly = "https://odqa.demos.ivoice.online/model";
	    	URL obj = new URL(urly);
	    	HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	
	    	con.setRequestMethod("POST");
	    	con.setRequestProperty("Content-Type","application/json");
	    	con.setRequestProperty("accept","application/json");
	
	    	con.setDoOutput(true);
	    	DataOutputStream wr = new DataOutputStream(con.getOutputStream());
	    	wr.writeBytes("{ \"context\": [ \"" +this.question+ "\" ]}");
	    	wr.flush();
	    	wr.close();
	
	    	BufferedReader iny = new BufferedReader(
	    	new InputStreamReader(con.getInputStream()));
	    	  String output;
	    	  StringBuffer response = new StringBuffer();
	
	    	  while ((output = iny.readLine()) != null) {
	    	   response.append(output);
	    	  }
	    	  
	    	  iny.close();
	    	  
	    	  //parse JSON
	    	  JSONParser parser = new JSONParser();
	          JSONArray array = (JSONArray)parser.parse(response.toString());	  
	          
	          //correct array 
		          array = (JSONArray) array.get(0);
		          array = (JSONArray) array.get(0);
	          //end correct
	          
		      System.out.println( array.get(0).toString());//out answer
		      
		      //show words to exit programm
		      String stringClose = "";
		      
		      for(int i = 0; i < commandClose.length; i++){
		    	  stringClose = stringClose + "[" + commandClose[i] + "]";
		      }
		      
	          System.out.println("Ask next question? Or send " + stringClose + " to exit the programm.");
	      
    	 }catch(Exception e){
    		 
    		 getContext().getLog().info(e.getMessage());
    	 }
    	
    	return Behaviors.stopped();

        
    }
}
