package com.lightbend.akka.sample;

import akka.actor.typed.ActorSystem;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
public class AkkaQuickstart {
  public static void main(String[] args) {
    //#actor-system
    final ActorSystem<GreeterMain.SayHello> greeterMain = ActorSystem.create(GreeterMain.create(), "helloakka");
    //#actor-system

    //var loop
    boolean close = false;
    
    //input string System in
    String inputString;
    
    //words to exit programm
    String commandClose[] = {"exit", "e", "quit", "close"};
    
    Scanner scanner;
    
    //first msg
    System.out.println("please, ask your question");
    
    try {
    	
    	while(!close){    		
    		
    		//scan console
    		scanner = new Scanner(System. in);
    		inputString = scanner. nextLine();  
    		
    		//if word equals commandClose then exit the programm
    		for(int i = 0; i < commandClose.length; i++){
    			
    			if(inputString.equals(commandClose[i])){
    				
    				System.out.println("command accept to exit. Have a nice day!");
    				close = true;    				
    				greeterMain.terminate();
    				System.exit(0);
    				
        		}  			
    			
    		}
    			
    		//#main-send-messages
    		//send question
    	    	greeterMain.tell(new GreeterMain.SayHello(inputString, commandClose));
    	    //#main-send-messages   	      			
			    		
    	}    	
    
    } catch (Exception e) {
    	System.out.println("OMG! We have ERROR!!! ->" + e.getMessage());
    } finally {
      greeterMain.terminate();
    }
  }
}
